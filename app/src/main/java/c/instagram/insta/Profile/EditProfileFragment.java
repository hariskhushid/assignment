package c.instagram.insta.Profile;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import c.instagram.insta.R;
import c.instagram.insta.Utils.FirebasedMethods;
import c.instagram.insta.Utils.UnivarsalImageLoader;
import c.instagram.insta.dialogs.ConfirmPasswordDialog;
import c.instagram.insta.models.UserSettings;
import c.instagram.insta.models.Users;
import c.instagram.insta.models.user_account_settings;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by h.khurshid on 12/20/2017.
 */

public class EditProfileFragment extends Fragment implements
        ConfirmPasswordDialog.OnConfirmPasswordListener {


    private static final String TAG = "Edit Profile Activity";
    private EditText mDisplayName, mUsername, mWebsite, mDescription, mEmail, mPhoneNumber;
    private CircleImageView mProfilePhoto;
    private ImageView mSaveChanges;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseDatabase mDb;
    private DatabaseReference mRef;
    private Context mContext = getActivity ();
    private FirebasedMethods fBm;
    private UserSettings mUserSettings;

    @Override
    public void onConfirmPassword(String password) {
        Log.d ( TAG, "onConfirmPassword: got the password: " + password );

        // Get auth credentials from the user for re-authentication. The example below shows
        // email and password credentials but there are multiple possible providers,
        // such as GoogleAuthProvider or FacebookAuthProvider.
        AuthCredential credential = EmailAuthProvider
                .getCredential ( mAuth.getCurrentUser ().getEmail (), password );

        ///////////////////// Prompt the user to re-provide their sign-in credentials
        mAuth.getCurrentUser ().reauthenticate ( credential )
                .addOnCompleteListener ( new OnCompleteListener<Void> () {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful ()) {
                            Log.d ( TAG, "User re-authenticated." );

                            ///////////////////////check to see if the email is not already present in the database
                            mAuth.fetchProvidersForEmail ( mEmail.getText ().toString () ).addOnCompleteListener ( new OnCompleteListener<ProviderQueryResult> () {
                                @Override
                                public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                                    if (task.isSuccessful ()) {
                                        try {
                                            if (task.getResult ().getProviders ().size () == 1) {
                                                Log.d ( TAG, "onComplete: that email is already in use." );
                                                Toast.makeText ( getActivity (), "That email is already in use", Toast.LENGTH_SHORT ).show ();
                                            } else {
                                                Log.d ( TAG, "onComplete: That email is available." );

                                                //////////////////////the email is available so update it
                                                mAuth.getCurrentUser ().updateEmail ( mEmail.getText ().toString () )
                                                        .addOnCompleteListener ( new OnCompleteListener<Void> () {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful ()) {
                                                                    Log.d ( TAG, "User email address updated." );
                                                                    Toast.makeText ( getActivity (), "email updated", Toast.LENGTH_SHORT ).show ();
                                                                    fBm.updateEmail ( mEmail.getText ().toString () );
                                                                }
                                                            }
                                                        } );
                                            }
                                        } catch (NullPointerException e) {
                                            Log.e ( TAG, "onComplete: NullPointerException: " + e.getMessage () );
                                        }
                                    }
                                }
                            } );


                        } else {
                            Log.d ( TAG, "onComplete: re-authentication failed." );
                        }

                    }
                } );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate ( R.layout.fragment_edit_profile, container, false );


        //initImageLoader ();
        //  setProfilePhoto ();
        mDisplayName = ( EditText ) v.findViewById ( R.id.DisplayName );
        mUsername = ( EditText ) v.findViewById ( R.id.DisplayUsername );
        mDescription = ( EditText ) v.findViewById ( R.id.DisplayDescription );
        mWebsite = ( EditText ) v.findViewById ( R.id.DisplayWebsite );
        mEmail = ( EditText ) v.findViewById ( R.id.DisplayEmail );
        mPhoneNumber = ( EditText ) v.findViewById ( R.id.DisplayPhoneNumber );
        mProfilePhoto = ( CircleImageView ) v.findViewById ( R.id.profilePhoto );
        mSaveChanges = ( ImageView ) v.findViewById ( R.id.saveChanges );
        fBm = new FirebasedMethods ( getActivity () );
        setUpFireBase ();

        ImageView backArrow = ( ImageView ) v.findViewById ( R.id.backArrow );
        backArrow.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                getActivity ().finish ();
            }
        } );

        mSaveChanges.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                saveProfileSettings ();
            }
        } );
        return v;
    }


    private void setProfilePhoto() {
        String imgUrl = "https://i.pinimg.com/originals/3a/92/04/3a92048181d5732bdb152318d1c0961f.png";
        UnivarsalImageLoader.setImage ( imgUrl, mProfilePhoto, null, "" );

    }

    private void saveProfileSettings(){
        final String displayName = mDisplayName.getText().toString();
        final String username = mUsername.getText().toString();
        final String website = mWebsite.getText().toString();
        final String description = mDescription.getText().toString();
        final String email = mEmail.getText().toString();
        final long phoneNumber = Long.parseLong(mPhoneNumber.getText().toString());


        //case1: if the user made a change to their username
        if(!mUserSettings.getUser().getUsername().equals(username)){

            checkIfUsernameExist(username);
        }
        //case2: if the user made a change to their email
        if(!mUserSettings.getUser().getEmail().equals(email)){

            // step1) Reauthenticate
            //          -Confirm the password and email
            ConfirmPasswordDialog dialog = new ConfirmPasswordDialog();
            dialog.show(getFragmentManager(), getString(R.string.confirm_password_dialog));
            dialog.setTargetFragment(EditProfileFragment.this, 1);


            // step2) check if the email already is registered
            //          -'fetchProvidersForEmail(String email)'
            // step3) change the email
            //          -submit the new email to the database and authentication
        }

        /**
         * change the rest of the settings that do not require uniqueness
         */
        if(!mUserSettings.getSettings().getDisplay_name().equals(displayName)){
            //update displayname
            fBm.updateUserAccountSettings(displayName, null, null, 0);
        }
        if(!mUserSettings.getSettings().getWebsite().equals(website)){
            //update website
            fBm.updateUserAccountSettings(null, website, null, 0);
        }
        if(!mUserSettings.getSettings().getDescription().equals(description)){
            //update description
            fBm.updateUserAccountSettings(null, null, description, 0);
        }
        if(mUserSettings.getUser ().getPhone_number ()!=phoneNumber){
            //update phoneNumber
            fBm.updateUserAccountSettings(null, null, null, phoneNumber);
        }
    }

    private void checkIfUsernameExist(final String userUsername) {
        DatabaseReference reference = FirebaseDatabase.getInstance ().getReference ();
        Query query = reference
                .child ( getString ( R.string.dbNames_user ) )
                .orderByChild ( getString ( R.string.field_username ) )
                .equalTo ( userUsername );
        query.addListenerForSingleValueEvent ( new ValueEventListener () {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists ()) {
                    for (DataSnapshot ds : dataSnapshot.getChildren ()) {
                        if (ds.exists ()) {
                            Toast.makeText ( getActivity (), " Username already exists ", Toast.LENGTH_LONG ).show ();
                        }
                    }
                } else {
                    fBm.updateUsername ( userUsername );
                    Toast.makeText ( getActivity (), " Username Saved ", Toast.LENGTH_LONG ).show ();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        } );
    }

    private void initWidgets(View v) {

        mDisplayName = ( EditText ) v.findViewById ( R.id.DisplayName );
        mUsername = ( EditText ) v.findViewById ( R.id.DisplayUsername );
        mDescription = ( EditText ) v.findViewById ( R.id.DisplayDescription );
        mWebsite = ( EditText ) v.findViewById ( R.id.DisplayWebsite );
        mEmail = ( EditText ) v.findViewById ( R.id.DisplayEmail );
        mPhoneNumber = ( EditText ) v.findViewById ( R.id.DisplayPhoneNumber );
        mProfilePhoto = ( CircleImageView ) v.findViewById ( R.id.profilePhoto );
    }

    private void setEditProfileWidgets(UserSettings settings) {
        Users objUser = settings.getUser ();
        user_account_settings uAc = settings.getSettings ();
        UnivarsalImageLoader.setImage ( uAc.getProfile_photo (), mProfilePhoto, null, "" );
        mUserSettings = settings;
        mUsername.setText ( objUser.getUsername () );
        mDisplayName.setText ( uAc.getDisplay_name () );
        mEmail.setText ( objUser.getEmail () );
        mPhoneNumber.setText ( String.valueOf ( objUser.getPhone_number () ) );


        mDescription.setText ( uAc.getDescription () );
        mWebsite.setText ( uAc.getWebsite () );

    }


    private void setUpFireBase() {
        mAuth = FirebaseAuth.getInstance ();
        mDb = FirebaseDatabase.getInstance ();
        mRef = mDb.getReference ();

        mAuthStateListener = new FirebaseAuth.AuthStateListener () {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser ();


                if (user != null) {
                    Log.d ( TAG, "onAuthStateChanged: Sign in :  " + user.getUid () );
                } else {
                    Log.d ( TAG, "onAuthStateChanged: sign out" );
                }
            }
        };
        mRef.addValueEventListener ( new ValueEventListener () {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                setEditProfileWidgets ( fBm.getUserSetting ( dataSnapshot ) );
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        } );
    }


    @Override
    public void onStart() {
        super.onStart ();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener ( mAuthStateListener );
        //updateUI ( mAuth.getCurrentUser () );
    }

    @Override
    public void onStop() {
        super.onStop ();
        if (mAuthStateListener != null) {
            mAuth.removeAuthStateListener ( mAuthStateListener );
        }
    }


}
