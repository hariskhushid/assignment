package c.instagram.insta.Profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;

import c.instagram.insta.R;
import c.instagram.insta.Utils.BottomNavigationViewHelper;
import c.instagram.insta.Utils.GridImageAdapter;
import c.instagram.insta.Utils.UnivarsalImageLoader;

/**
 * Created by h.khurshid on 12/19/2017.
 */

public class ProfileActivity extends AppCompatActivity {

    private String TAG="Profile Activity";
    private static final int ACTIVITY_NUMBER = 4;
    private Context mContex = ProfileActivity.this;
    private ImageView profilePhoto;
    private ProgressBar progressBar;
    private static final int NUM_GRID_COLLUMNS=3;

    @Override
    protected void onCreate(@Nullable Bundle saveInstanceState) {
        super.onCreate ( saveInstanceState );
        setContentView ( R.layout.activity_profile );
        init ();

//        setUpBottomNavigationView ();
//        setUpToolbar ();
//        setUpActivityWidgets ();
//        setProfilePhoto ();
//        tempGridSetup();
    }
    private void init(){
       Log.d ( TAG," inflating to "+R.string.profile_fragment );
       ProfileFragment profileFragment= new ProfileFragment ();
        FragmentTransaction fragmentTransaction=null;
        fragmentTransaction= ProfileActivity.this.getSupportFragmentManager ().beginTransaction ();
        fragmentTransaction.replace(R.id.container,profileFragment);
        fragmentTransaction.addToBackStack ( getString ( R.string.profile_fragment ) );
        fragmentTransaction.commit ();

    }

   /* private void setProfilePhoto() {
        String imgUrl = "i.pinimg.com/originals/3a/92/04/3a92048181d5732bdb152318d1c0961f.png";
        UnivarsalImageLoader.setImage ( imgUrl, profilePhoto, progressBar, "https://" );
    }

    private void setUpActivityWidgets() {
        progressBar = ( ProgressBar ) findViewById ( R.id.prifileProgressBar );
        progressBar.setVisibility ( View.GONE );

        profilePhoto = ( ImageView ) findViewById ( R.id.profileImage );
    }
    private void tempGridSetup(){
    ArrayList<String> urls= new ArrayList<String> (  );
    urls.add ("i.pinimg.com/originals/3a/92/04/3a92048181d5732bdb152318d1c0961f.png"  );
    urls.add ("cdn2.techadvisor.co.uk/cmsdata/features/3614881/Android_thumb800.jpg"  );
    urls.add ("assets.pcmag.com/media/images/535129-things-you-can-do-to-make-your-android-device-less-annoying-right-now.png?thumb=y&width=810&height=456"  );
    urls.add ("tr2.cbsistatic.com/hub/i/r/2017/01/31/7e355c52-c68f-4389-825f-392f2dd2ac19/resize/770x/d19d6c021f770122da649e2a77bd1404/androiddatahero.jpg"  );
    urls.add ("images.techhive.com/images/article/2016/09/android-old-habits-100682662-primary.idge.jpg"  );
    urls.add ("www1-lw.xda-cdn.com/files/2017/08/Android-P-810x298_c.png"  );
    urls.add ("i.pinimg.com/originals/3a/92/04/3a92048181d5732bdb152318d1c0961f.png"  );
    setupImageGrid ( urls );
}
    private void setupImageGrid(ArrayList<String> imgUrl) {
        GridView imgGrid = ( GridView ) findViewById ( R.id.imgGridView );
        int gridWidth=getResources ().getDisplayMetrics ().widthPixels;
        int imgWidth=gridWidth/NUM_GRID_COLLUMNS;
        imgGrid.setColumnWidth ( imgWidth );

        GridImageAdapter imageAdapter= new GridImageAdapter (  mContex,R.layout.layout_grid_image_view,"https://",imgUrl);
        imgGrid.setAdapter ( imageAdapter );
    }

    private void setUpToolbar() {
        Toolbar upToolbar = ( Toolbar ) findViewById ( R.id.profileToolBar );
        setSupportActionBar ( upToolbar );
        ImageView profileMenu = ( ImageView ) findViewById ( R.id.profileMenu );
        profileMenu.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                startActivity ( new Intent ( mContex, AccountSettingActivity.class ) );
            }
        } );

    }

    private void setUpBottomNavigationView() {

        BottomNavigationViewEx bottomNavigationViewEx = ( BottomNavigationViewEx ) findViewById ( R.id.bottomNavViewBar );
        BottomNavigationViewHelper.setUpBottomNavigationView ( bottomNavigationViewEx );
        BottomNavigationViewHelper.enableNavigation ( mContex, bottomNavigationViewEx );
        Menu menu = bottomNavigationViewEx.getMenu ();
        MenuItem menuItem = menu.getItem ( ACTIVITY_NUMBER );
        menuItem.setChecked ( true );

    }*/


}
