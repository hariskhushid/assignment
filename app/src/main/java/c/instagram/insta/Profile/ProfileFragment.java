package c.instagram.insta.Profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import c.instagram.insta.Authencation.LoginActivity;
import c.instagram.insta.R;
import c.instagram.insta.Utils.BottomNavigationViewHelper;
import c.instagram.insta.Utils.FirebasedMethods;
import c.instagram.insta.Utils.UnivarsalImageLoader;
import c.instagram.insta.models.UserSettings;
import c.instagram.insta.models.Users;
import c.instagram.insta.models.user_account_settings;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by h.khurshid on 12/27/2017.
 */

public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileFragment";

    private static final int ACTIVITY_NUM = 4;


    private TextView mPosts, mFollowers, mFollowing, mDisplayName, mUsername, mWebsite, mDescription,mEditProfile;
    private ProgressBar mProgressBar;
    private CircleImageView mProfilePhoto;
    private GridView gridView;
    private Toolbar toolbar;
    private ImageView profileMenu;
    private BottomNavigationViewEx bottomNavigationView;

    private Context mContext;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseDatabase mDb;
    private DatabaseReference mRef;
    // private Context mContex = getActivity ();
    private FirebasedMethods fBm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate ( R.layout.fragment_profile, container, false );
        mDisplayName = ( TextView ) view.findViewById ( R.id.profileName );
        mUsername = ( TextView ) view.findViewById ( R.id.profileName );
        mWebsite = ( TextView ) view.findViewById ( R.id.website );
        mDescription = ( TextView ) view.findViewById ( R.id.description );
        mProfilePhoto = ( CircleImageView ) view.findViewById ( R.id.profileImage );
        mPosts = ( TextView ) view.findViewById ( R.id.tvPosts );
        mFollowers = ( TextView ) view.findViewById ( R.id.tvFollower );
        mFollowing = ( TextView ) view.findViewById ( R.id.tvFollowing );
        mProgressBar = ( ProgressBar ) view.findViewById ( R.id.profileProgressBar );
        gridView = ( GridView ) view.findViewById ( R.id.gridImageView );
        toolbar = ( Toolbar ) view.findViewById ( R.id.profileToolBar );
        profileMenu = ( ImageView ) view.findViewById ( R.id.profileMenu );
        bottomNavigationView = ( BottomNavigationViewEx ) view.findViewById ( R.id.bottomNavViewBar );
        mEditProfile=(TextView )view.findViewById ( R.id.textViewEditProfile );
        mContext = getActivity ();
        Log.d ( TAG, "onCreateView: stared." );

        fBm = new FirebasedMethods ( mContext );
        setupBottomNavigationView ();
        setupToolbar ();
         setUpFireBase ();
         mEditProfile.setOnClickListener ( new View.OnClickListener () {
             @Override
             public void onClick(View view) {
                 Intent intent = new Intent ( getActivity (),AccountSettingActivity.class );
                 intent.putExtra ( getString (R.string.calling_activity),getString ( R.string.profile_activity ) );
                 startActivity ( intent );
             }
         } );
        return view;
    }

    private void setProfileWidgets(UserSettings settings) {
        Users users=settings.getUser();
        user_account_settings userAccountSettings = settings.getSettings ();
        // String  imgUrl=;
        //String  imgUrl1="https://i.pinimg.com/originals/3a/92/04/3a92048181d5732bdb152318d1c0961f.png";
        UnivarsalImageLoader.setImage ( userAccountSettings.getProfile_photo (), mProfilePhoto, null, "" );
        mUsername.setText ( users.getUsername () );
        mWebsite.setText ( userAccountSettings.getWebsite () );
        mDescription.setText ( userAccountSettings.getDescription () );
        mPosts.setText ( String.valueOf ( userAccountSettings.getPosts () ) );
        mFollowers.setText ( String.valueOf ( userAccountSettings.getFollowers () ) );
        mFollowing.setText ( String.valueOf ( userAccountSettings.getFollowings () ) );
        mProgressBar.setVisibility ( View.GONE );
    }


    private void setupToolbar() {

        (( ProfileActivity ) getActivity ()).setSupportActionBar ( toolbar );

        profileMenu.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Log.d ( TAG, "onClick: navigating to account settings." );
                Intent intent = new Intent ( mContext, AccountSettingActivity.class );
                startActivity ( intent );
            }
        } );
    }

    private void setupBottomNavigationView() {
        Log.d ( TAG, "setupBottomNavigationView: setting up BottomNavigationView" );
        BottomNavigationViewHelper.setUpBottomNavigationView ( bottomNavigationView );
        BottomNavigationViewHelper.enableNavigation ( mContext, bottomNavigationView );
        Menu menu = bottomNavigationView.getMenu ();
        MenuItem menuItem = menu.getItem ( ACTIVITY_NUM );
        menuItem.setChecked ( true );
    }

    private void setUpFireBase() {
        mAuth = FirebaseAuth.getInstance ();
        mDb = FirebaseDatabase.getInstance ();
        mRef = mDb.getReference ();
        mAuthStateListener = new FirebaseAuth.AuthStateListener () {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser ();
                updateUI ( user );

                if (user != null) {
                    Log.d ( TAG, "onAuthStateChanged: Sign in :  " + user.getUid () );
                } else {
                    Log.d ( TAG, "onAuthStateChanged: sign out" );
                }
            }
        };
        mRef.addValueEventListener ( new ValueEventListener () {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                setProfileWidgets ( fBm.getUserSetting ( dataSnapshot ) );
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        } );
    }


    private void updateUI(FirebaseUser user) {
        if (user == null) {

            Toast.makeText ( mContext, " no user found", Toast.LENGTH_LONG );
            Intent intent = new Intent ( mContext, LoginActivity.class );
            startActivity ( intent );

        }

    }

    @Override
    public void onStart() {
        super.onStart ();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener ( mAuthStateListener );
        //updateUI ( mAuth.getCurrentUser () );
    }

    @Override
    public void onStop() {
        super.onStop ();
        if (mAuthStateListener != null) {
            mAuth.removeAuthStateListener ( mAuthStateListener );
        }
    }
}
