package c.instagram.insta.Profile;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import c.instagram.insta.Authencation.LoginActivity;
import c.instagram.insta.R;

/**
 * Created by h.khurshid on 12/20/2017.
 */

public class SignOutFragment extends Fragment {
    private String TAG="Sign out fragment";
    private Context mContex=this.getContext ();
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListner;
    private ProgressBar mProgressBar;
    private TextView waitTextView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate ( R.layout.fragment_signout,container,false );

        mProgressBar=(ProgressBar) view.findViewById ( R.id.mProgressBar );
        waitTextView=(TextView) view.findViewById ( R.id.tvWait );
        waitTextView.setVisibility ( View.GONE );
        mProgressBar.setVisibility ( View.GONE );
        setUpFireBase();
        Button btnSignout=(Button) view.findViewById ( R.id.btnConfirmSignOut );
        btnSignout.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                waitTextView.setVisibility ( View.VISIBLE );
                mProgressBar.setVisibility ( View.VISIBLE );
                mAuth.signOut ();
                getActivity ().finish ();
            }
        } );
        return view;
    }
    private void setUpFireBase(){
        mAuth=FirebaseAuth.getInstance ();
        mAuthListner=new FirebaseAuth.AuthStateListener () {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user= firebaseAuth.getCurrentUser ();


                if(user!=null){
                    Log.d ( TAG, "onAuthStateChanged: Sign in :  "+user.getUid ());
                }else{
                    Log.d ( TAG, "onAuthStateChanged: sign out" );
                    Log.d ( TAG, "onAuthStateChanged: navigating to login activity" );
                    Intent intent=new Intent ( getActivity (),LoginActivity.class );
                    intent.setFlags ( Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK );
                    startActivity ( intent );
                }
            }
        };
    }


    @Override
    public void onStart() {
        super.onStart ();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener ( mAuthListner );
        //updateUI ( mAuth.getCurrentUser () );
    }

    @Override
    public void onStop() {
        super.onStop ();
        if(mAuthListner!=null){
            mAuth.removeAuthStateListener ( mAuthListner );
        }
    }
}
