package c.instagram.insta.Profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;

import c.instagram.insta.Authencation.LoginActivity;
import c.instagram.insta.R;
import c.instagram.insta.Utils.BottomNavigationViewHelper;
import c.instagram.insta.Utils.FirebasedMethods;
import c.instagram.insta.Utils.SectionStatePagerAdapter;
import c.instagram.insta.models.UserSettings;

/**
 * Created by h.khurshid on 12/20/2017.
 */

public class AccountSettingActivity extends AppCompatActivity {
    private static final String TAG = "AccountSettingActivity";
    private final Context mContext = AccountSettingActivity.this;
    private SectionStatePagerAdapter sectionStatePagerAdapter;
    private ViewPager viewPager;
    private RelativeLayout relativeLayout;
    private static final int ACTIVITY_NUMBER = 4;
   //private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_accountsetting );
        setUpBottomNavigationView ();
        viewPager = ( ViewPager ) findViewById ( R.id.container );
        relativeLayout = ( RelativeLayout ) findViewById ( R.id.relLayout1 );
        setUpSettingsList ();
        setUpFragments ();
        getCommingIntents ();
        ImageView backArrow = ( ImageView ) findViewById ( R.id.backArrow );
        backArrow.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                finish ();
            }
        } );
    }

    private void getCommingIntents() {
        Intent intent
                = getIntent ();
        if (intent.hasExtra ( getString ( R.string.calling_activity ) )) {
            setViewPager ( sectionStatePagerAdapter.getFragmentNumber ( getString ( R.string.edit_profile_fragrement ) ) );
        }
    }

    private void setUpFragments() {
        sectionStatePagerAdapter = new SectionStatePagerAdapter ( getSupportFragmentManager () );
        sectionStatePagerAdapter.addFragment ( new EditProfileFragment (), "Edit Profile" );
        sectionStatePagerAdapter.addFragment ( new SignOutFragment (), "Sign Out" );

    }

    private void setViewPager(int fragmentNumber) {
        relativeLayout.setVisibility ( View.GONE );
        viewPager.setAdapter ( sectionStatePagerAdapter );
        viewPager.setCurrentItem ( fragmentNumber );

    }

    private void setUpSettingsList() {
        ListView listView = ( ListView ) findViewById ( R.id.lsViewAccountSettig );
        ArrayList<String> options = new ArrayList<String> ();
        options.add ( "Edit Profile" );
        options.add ( "Sign Out" );

        ArrayAdapter arrayAdapter = new ArrayAdapter ( mContext, android.R.layout.simple_list_item_1, options );
        listView.setAdapter ( arrayAdapter );

        listView.setOnItemClickListener ( new AdapterView.OnItemClickListener () {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                setViewPager ( position );
            }
        } );
    }

    private void setUpBottomNavigationView() {

        BottomNavigationViewEx bottomNavigationViewEx;
        bottomNavigationViewEx = ( BottomNavigationViewEx ) findViewById ( R.id.bottomNavViewBar );
        BottomNavigationViewHelper.setUpBottomNavigationView ( bottomNavigationViewEx );
        BottomNavigationViewHelper.enableNavigation ( mContext, bottomNavigationViewEx );
        Menu menu = bottomNavigationViewEx.getMenu ();
        MenuItem menuItem = menu.getItem ( ACTIVITY_NUMBER );
        menuItem.setChecked ( true );


    }

}
