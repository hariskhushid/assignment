package c.instagram.insta.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by h.khurshid on 12/20/2017.
 */


public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private  final List<Fragment> mFragmenList= new ArrayList<Fragment> (  );
    public SectionsPagerAdapter(FragmentManager fm) {
        super ( fm );
    }

    @Override
    public Fragment getItem(int position) {

        return mFragmenList.get ( position );
    }

    @Override
    public int getCount() {
        return mFragmenList.size ();
    }
    public void addFragment(Fragment fragment){
        mFragmenList.add ( fragment );
    }
}
