package c.instagram.insta.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import c.instagram.insta.R;

/**
 * Created by h.khurshid on 12/21/2017.
 */

public class GridImageAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private LayoutInflater inflater;
    private int layoutResource;
    private String append;
    private ArrayList<String> imgUrls;

    public GridImageAdapter(Context mContext,  int layoutResource, String append, ArrayList<String> imgUrls) {
       super(mContext,layoutResource,imgUrls);
        this.mContext = mContext;
        inflater=(LayoutInflater ) mContext.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
        this.layoutResource = layoutResource;
        this.append = append;
        this.imgUrls = imgUrls;
    }
    private static class ViewHolder{
        SquareImageView profileImages;
        ProgressBar mProgressBar;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        if(convertView==null){
            convertView=inflater.inflate ( layoutResource,parent,false );
            holder=new ViewHolder ();
            holder.mProgressBar=(ProgressBar) convertView.findViewById ( R.id.gridImageProgressBar );
            holder.profileImages=( SquareImageView ) convertView.findViewById ( R.id.gridImageView );
            convertView.setTag ( holder );

        }
        else {
            holder=(ViewHolder) convertView.getTag();
        }
        String imgUrl=getItem ( position );
        ImageLoader imageLoader=ImageLoader.getInstance ();
        imageLoader.displayImage ( append + imgUrl, holder.profileImages, new ImageLoadingListener () {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                if (holder.mProgressBar != null) {
                    holder.mProgressBar.setVisibility ( View.VISIBLE );
                }
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                if (holder.mProgressBar != null) {
                    holder.mProgressBar.setVisibility ( View.GONE );
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (holder.mProgressBar != null) {
                    holder.mProgressBar.setVisibility ( View.GONE );
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                if (holder.mProgressBar != null) {
                    holder.mProgressBar.setVisibility ( View.GONE );
                }
            }
        } );
        return convertView;
    }
}
