package c.instagram.insta.Utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import c.instagram.insta.R;
import c.instagram.insta.models.UserSettings;
import c.instagram.insta.models.Users;
import c.instagram.insta.models.user_account_settings;

/**
 * Created by h.khurshid on 12/21/2017.
 */

public class FirebasedMethods {
    private static final String TAG = "Firebased Methods";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private Context mContext;
    private String userId;
    private FirebaseDatabase mDb;
    private DatabaseReference mRef;


    public FirebasedMethods(Context ctx) {
        this.mContext = ctx;
        this.userId = "";
        mAuth = FirebaseAuth.getInstance ();
        mDb = FirebaseDatabase.getInstance ();
        mRef = mDb.getReference ();
        if (mAuth.getCurrentUser () != null) {
            userId = mAuth.getCurrentUser ().getUid ();
        }
    }

    public void registerEmail(final String email, final String password) {
        mAuth.createUserWithEmailAndPassword ( email, password )
                .addOnCompleteListener ( new OnCompleteListener<AuthResult> () {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful ()) {

                            Log.d ( TAG, "signInWithEmail:success" );
                            //  FirebaseUser user = mAuth.getCurrentUser ();
                            sendVerificationemail ();
                            userId = mAuth.getCurrentUser ().getUid ();


                        } else {

                            Log.w ( TAG, "signInWithEmail:failure", task.getException () );
                            Toast.makeText ( mContext, "Registration fails", Toast.LENGTH_LONG ).show ();

                        }

                        // ...
                    }

                } );
        //return userId;
    }

//    public boolean checkIfUsernameExists(String username, DataSnapshot snapshot) {
//
//        Users usr = new Users ();
//        for (DataSnapshot ds : snapshot.child ( userId ).getChildren ()) {
//            usr.setUsername ( ds.getValue ( Users.class ).getUsername () );
//
//            if (StringManipulation.expandUsername ( usr.getUsername () ).equals ( username )) {
//                return true;
//            }
//        }
//        return false;
//    }

    public void addNewUser(String email, String username,String displayName, long phone_number, String description, String website, long followers, long followings, long posts, String profilePhoto) {
        Users user = new Users ( userId, email, StringManipulation.condenseUsername ( username ),displayName, phone_number );
        mRef.child ( mContext.getString ( R.string.dbNames_user ) ).child ( userId )
                .setValue ( user );
        user_account_settings account_setting = new user_account_settings (
                username, description, website, followers, followers, posts, profilePhoto
        );
        mRef.child ( mContext.getString ( R.string.dbNames_userAccountSettings ) )
                .child ( userId ).setValue ( account_setting );


    }

    public void sendVerificationemail() {
        FirebaseUser user = FirebaseAuth.getInstance ().getCurrentUser ();
        if (user != null) {
            user.sendEmailVerification ().addOnCompleteListener ( new OnCompleteListener<Void> () {

                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (!task.isSuccessful ()) {
                        Toast.makeText ( mContext, "Could not send verification email", Toast.LENGTH_LONG ).show ();

                    }
                }
            } );
        }
    }

    /**
     * @param snapshot
     * @return
     */
    public UserSettings getUserSetting(DataSnapshot snapshot) {
        user_account_settings settings = new user_account_settings ();
        Users objUser = new Users ();
        for (DataSnapshot ds : snapshot.getChildren ()) {
            if (ds.getKey ().equals ( mContext.getString ( R.string.dbNames_userAccountSettings ) )) {
                try {
                    settings.setDisplay_name ( ds.child ( userId )
                            .getValue ( user_account_settings.class )
                            .getDisplay_name () );

                    settings.setWebsite ( ds.child ( userId )
                            .getValue ( user_account_settings.class )
                            .getWebsite () );
                    settings.setDescription ( ds.child ( userId )
                            .getValue ( user_account_settings.class )
                            .getDescription () );
                    settings.setProfile_photo ( ds.child ( userId )
                            .getValue ( user_account_settings.class )
                            .getProfile_photo () );
                    settings.setPosts ( ds.child ( userId )
                            .getValue ( user_account_settings.class )
                            .getPosts () );
                    settings.setFollowings ( ds.child ( userId )
                            .getValue ( user_account_settings.class )
                            .getFollowings () );
                    settings.setFollowers ( ds.child ( userId )
                            .getValue ( user_account_settings.class )
                            .getFollowers () );
                } catch (NullPointerException e) {
                    Log.e ( TAG, "getUseraccountsettings  Null pointer execption " + e );
                }


            }
            if (ds.getKey ().equals ( mContext.getString ( R.string.dbNames_user ) )) {
                try {
                    objUser.setFull_name ( ds.child ( userId )
                            .getValue ( Users.class )
                            .getFull_name () );
                    objUser.setUsername ( ds.child ( userId )
                            .getValue ( Users.class )
                            .getUsername () );
                    objUser.setUsername ( ds.child ( userId )
                            .getValue ( Users.class )
                            .getUsername () );
                    objUser.setEmail ( ds.child ( userId )
                            .getValue ( Users.class )
                            .getEmail () );
                    objUser.setPhone_number ( ds.child ( userId )
                            .getValue ( Users.class )
                            .getPhone_number () );
                    objUser.setUserId ( ds.child ( userId )
                            .getValue ( Users.class )
                            .getUserId () );
                } catch (NullPointerException e) {
                    Log.e ( TAG, "getUseraccountsettings  Null pointer execption " + e );
                }
            }
        }
        return new UserSettings ( objUser, settings );
    }

//    public void EditUserInformation( Users user,user_account_settings account_setting){
//
//        mRef.child ( mContext.getString ( R.string.dbNames_user ) ).child ( userId )
//                .setValue ( user );
//
//        mRef.child ( mContext.getString ( R.string.dbNames_userAccountSettings ) )
//                .child ( userId ).setValue ( account_setting );
//    }

    public void updateUsername(String userUsername) {

        mRef.child ( mContext.getString ( R.string.dbNames_user ) )
                .child ( userId )
                .child(mContext.getString ( R.string.field_username ))
                .setValue ( userUsername );
      /*  mRef.child ( mContext.getString ( R.string.dbNames_userAccountSettings ) )
                .child ( userId )
                .child(mContext.getString ( R.string.field_username ))
                .setValue ( userUsername );*/



    }
    public void updateEmail(String email){
        mRef.child ( mContext.getString ( R.string.dbNames_user ) )
                .child ( userId )
                .child(mContext.getString ( R.string.field_email ))
                .setValue ( email );

    }
    public void updateUserAccountSettings(String displayName,String website,String description,long phoneNumber){
        Log.e ( TAG, "getUseraccountsettings  updating user account setting " );
        if(displayName!=null) {

            mRef.child ( mContext.getString ( R.string.dbNames_userAccountSettings ) )
                    .child ( userId )
                    .child ( mContext.getString ( R.string.field_displayname ) )
                    .setValue ( displayName );
        }
        if(website!=null) {
            mRef.child ( mContext.getString ( R.string.dbNames_userAccountSettings ) )
                    .child ( userId )
                    .child ( mContext.getString ( R.string.field_website ) )
                    .setValue ( website );
        }
        if(description!=null) {
            mRef.child ( mContext.getString ( R.string.dbNames_userAccountSettings ) )
                    .child ( userId )
                    .child ( mContext.getString ( R.string.field_description ) )
                    .setValue ( description );
        }
        if(phoneNumber!=0) {
            mRef.child ( mContext.getString ( R.string.dbNames_user ) )
                    .child ( userId )
                    .child ( mContext.getString ( R.string.field_phonenumber ) )
                    .setValue ( phoneNumber );
        }
    }
}
