package c.instagram.insta.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by h.khurshid on 12/20/2017.
 */

public class SectionStatePagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentsList = new ArrayList<Fragment> ();
    private final HashMap<Fragment, Integer> mFragment = new HashMap<> ();
    private final HashMap<String, Integer> mFragmentNumber = new HashMap<> ();
    private final HashMap<Integer, String> mFragmentName = new HashMap<> ();

    public SectionStatePagerAdapter(FragmentManager fm) {
        super ( fm );
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentsList.get ( position );
    }

    @Override
    public int getCount() {
        return mFragmentsList.size ();
    }

    public void addFragment(Fragment fragment, String fragmentName) {
        mFragmentsList.add ( fragment );
        mFragment.put ( fragment, mFragmentsList.size () - 1 );
        mFragmentNumber.put ( fragmentName, mFragmentsList.size () - 1 );
        mFragmentName.put ( mFragmentsList.size () - 1, fragmentName );
    }

    public Integer getFragmentNumber(String fragmentName) {
        if (mFragmentNumber.containsKey ( fragmentName )) {
            return mFragmentNumber.get ( fragmentName );
        } else {
            return null;
        }
    }

    public Integer getFragmentNumber(Fragment fragment) {
        if (mFragmentNumber.containsKey ( fragment )) {
            return mFragmentNumber.get ( fragment );
        } else {
            return null;
        }
    }

    public String getFragmentNumber(Integer fragmentNumber) {
        if (mFragmentNumber.containsKey ( fragmentNumber )) {
            return mFragmentName.get ( fragmentNumber );
        } else {
            return null;
        }
    }
}
