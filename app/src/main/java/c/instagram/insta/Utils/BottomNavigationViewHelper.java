package c.instagram.insta.Utils;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import c.instagram.insta.Home.HomeActivity;
import c.instagram.insta.Likes.LikesActivity;
import c.instagram.insta.Profile.ProfileActivity;
import c.instagram.insta.R;
import c.instagram.insta.Search.SearchActivity;
import c.instagram.insta.Share.ShareActivity;

/**
 * Created by h.khurshid on 12/19/2017.
 */

public class BottomNavigationViewHelper {
    public static final String TAG = "BottomNavigationViewHel";

    public static void setUpBottomNavigationView(BottomNavigationViewEx navigationViewEx) {
        Log.d ( TAG, "setting up bottomnavigationview: " );
        navigationViewEx.enableAnimation ( Boolean.FALSE );
        navigationViewEx.enableItemShiftingMode ( Boolean.FALSE );
        navigationViewEx.enableShiftingMode ( Boolean.FALSE );
        navigationViewEx.setTextVisibility ( Boolean.FALSE );
    }

    public static void enableNavigation(final Context ctx, BottomNavigationViewEx bottomNavigationViewEx) {
        bottomNavigationViewEx.setOnNavigationItemSelectedListener ( new BottomNavigationView.OnNavigationItemSelectedListener () {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId ()) {


                    case R.id.ic_house: {
                        Intent intent = new Intent ( ctx, HomeActivity.class );
                        ctx.startActivity ( intent );
                        break;
                    }
                    case R.id.ic_alert: {
                        Intent intent = new Intent ( ctx, LikesActivity.class );
                        ctx.startActivity ( intent );
                        break;
                    }
                    case R.id.ic_android: {
                        Intent intent = new Intent ( ctx, ProfileActivity.class );
                        ctx.startActivity ( intent );
                        break;
                    }
                    case R.id.ic_circle: {
                        Intent intent = new Intent ( ctx, ShareActivity.class );
                        ctx.startActivity ( intent );
                        break;
                    }
                    case R.id.ic_search: {
                        Intent intent = new Intent ( ctx, SearchActivity.class );
                        ctx.startActivity ( intent );
                        break;
                    }

                }

                return false;
            }
        } );
    }
}
