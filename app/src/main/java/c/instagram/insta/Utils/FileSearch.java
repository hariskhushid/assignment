package c.instagram.insta.Utils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by h.khurshid on 1/18/2018.
 */

public class FileSearch {
    public static ArrayList<String> getDirectoryPath(String directory ){

        ArrayList<String> pathsArray= new ArrayList<> (  );
        File file= new File ( directory );
        File[] listFiles=file.listFiles ();
        for (int i=0;i<listFiles.length;i++){
           if(listFiles[i].isDirectory ()){
               pathsArray.add ( listFiles[i].getAbsolutePath () );
           }
        }
        return pathsArray;
    }
    public static ArrayList<String> getFilesPath(String directory){
        ArrayList<String> fileArray= new ArrayList<> (  );
        File file= new File ( directory );
        File[] listFiles=file.listFiles ();
        for (int i=0;i<listFiles.length;i++){
            if(listFiles[i].isFile ()){
                fileArray.add ( listFiles[i].getAbsolutePath () );
            }
        }
        return fileArray;
    }
}
