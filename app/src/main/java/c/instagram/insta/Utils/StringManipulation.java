package c.instagram.insta.Utils;

/**
 * Created by h.khurshid on 12/25/2017.
 */

public class StringManipulation {
    public static String expandUsername(String username){
        return username.replace ( "."," " );
    }
    public static String condenseUsername(String username){
        return username.replace ( " ","." );
    }
}
