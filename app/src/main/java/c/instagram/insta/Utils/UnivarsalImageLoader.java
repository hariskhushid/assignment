package c.instagram.insta.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import c.instagram.insta.R;

/**
 * Created by h.khurshid on 12/20/2017.
 */

public class UnivarsalImageLoader {
    public static final int defaultImage = R.drawable.ic_android;
    private Context mContext;

    public UnivarsalImageLoader(Context mContext) {
        this.mContext = mContext;
    }

    public static void setImage(String imgUrl, ImageView imageView, final ProgressBar progressBar, String append) {
        ImageLoader imageLoader = ImageLoader.getInstance ();
        imageLoader.displayImage ( append + imgUrl, imageView, new ImageLoadingListener () {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                if (progressBar != null) {
                    progressBar.setVisibility ( View.VISIBLE );
                }
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                if (progressBar != null) {
                    progressBar.setVisibility ( View.GONE );
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (progressBar != null) {
                    progressBar.setVisibility ( View.GONE );
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                if (progressBar != null) {
                    progressBar.setVisibility ( View.GONE );
                }
            }
        } );

    }

    public ImageLoaderConfiguration getConfig() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder ()
                .showImageForEmptyUri ( defaultImage )
                .showImageOnFail ( defaultImage )
                .showImageOnLoading ( defaultImage )
                .cacheOnDisk ( true )
                .cacheInMemory ( true )
                .resetViewBeforeLoading ( true )
                .imageScaleType ( ImageScaleType.EXACTLY )
                .displayer ( new FadeInBitmapDisplayer ( 300 ) ).build ();
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder ( mContext )
                .defaultDisplayImageOptions ( defaultOptions )
                .memoryCache ( new WeakMemoryCache () )
                .diskCacheSize ( 100 * 1024 * 1024 ).build ();
        return configuration;


    }

}
