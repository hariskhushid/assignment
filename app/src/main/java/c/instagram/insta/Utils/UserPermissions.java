package c.instagram.insta.Utils;


import android.Manifest;

/**
 * Created by h.khurshid on 1/18/2018.
 */

public class UserPermissions {
    public static final String[] PERMISSIONS={
           Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };public static final String[] CAM_PERMISSIONS={

            Manifest.permission.CAMERA,
    };public static final String[] WRITE_STORAGE_PERMISSIONS={

            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };public static final String[] READ_STORAGE_PERMISSIONS={

            Manifest.permission.READ_EXTERNAL_STORAGE
    };

}
