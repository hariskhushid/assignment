package c.instagram.insta.Home;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import c.instagram.insta.R;

/**
 * Created by h.khurshid on 12/20/2017.
 */

public class HomeFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate ( R.layout.fragment_home,container,false );
        return view;
    }
  /*  public void createKMLFile(){



        String kmlstart = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n";

        String kmlelement ="\t<Placemark>\n" +
                "\t<name>Simple placemark</name>\n" +
                "\t<description>"+" Haris "+"</description>\n" +
                "\t<Point>\n" +
                "\t\t<coordinates>"+"33.1212121"+","+"-73.2554555"+","+ "</coordinates>\n" +
                "\t\t<coordinates>"+"33.1212121"+","+"-73.2554555"+","+ "</coordinates>\n" +
                "\t</Point>\n" +
                "\t</Placemark>\n";

        String kmlend = "</kml>";

        ArrayList<String> content = new ArrayList<String>();
        content.add(0,kmlstart);
        content.add(1,kmlelement);
        content.add(2,kmlend);

        String kmltest = content.get(0) + content.get(1) + content.get(2);


        File testexists = new File(""+"/"+""+".kml");
        Writer fwriter;

        if(!testexists.exists()){
            try {

                fwriter = new FileWriter ("") {
                    @Override
                    public void write(@NonNull char[] chars, int i, int i1) throws IOException {

                    }

                    @Override
                    public void flush() throws IOException {

                    }

                    @Override
                    public void close() throws IOException {

                    }
                };
                fwriter.write(kmltest);
                fwriter.flush();
                fwriter.close();
            }catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }

        else{

            //schleifenvariable
            String filecontent ="";

            ArrayList<String> newoutput = new ArrayList<String>();;

            try {
                BufferedReader in = new BufferedReader(new FileReader (testexists));
                while((filecontent = in.readLine()) !=null)

                    newoutput.add(filecontent);

            } catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            newoutput.add(2,kmlelement);

            String rewrite ="";
            for(String s : newoutput){
                rewrite += s;
            }

            try {
                fwriter = new FileWriter(""+"/"+""+".kml");
                fwriter.write(rewrite);
                fwriter.flush();
                fwriter.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }*/
}
