package c.instagram.insta.Home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.nostra13.universalimageloader.core.ImageLoader;

import c.instagram.insta.Authencation.LoginActivity;
import c.instagram.insta.R;
import c.instagram.insta.Utils.BottomNavigationViewHelper;
import c.instagram.insta.Utils.SectionsPagerAdapter;
import c.instagram.insta.Utils.UnivarsalImageLoader;

public class HomeActivity extends AppCompatActivity {
    private static final int ACTIVITY_NUMBER = 0;
    private static final String TAG = "Home Activity";
    private Context mContex = HomeActivity.this;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate ( savedInstanceState );

        setContentView ( R.layout.activity_home );
        setUpFireBase ();
        initImageLoader ();
        setUpBottomNavigationView ();
        setupViewPager ();
        //mAuth.signOut ();
    }

    private void setUpFireBase() {
        mAuth = FirebaseAuth.getInstance ();
        mAuthStateListener = new FirebaseAuth.AuthStateListener () {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser ();
                updateUI ( user );

                if (user != null) {
                    Log.d ( TAG, "onAuthStateChanged: Sign in :  " + user.getUid () );
                } else {
                    Log.d ( TAG, "onAuthStateChanged: sign out" );
                }
            }
        };
    }

    private void updateUI(FirebaseUser user) {
        if (user == null) {

            Toast.makeText ( mContex, " no user found", Toast.LENGTH_LONG );
            Intent intent = new Intent ( mContex, LoginActivity.class );
            startActivity ( intent );

        }

    }

    @Override
    public void onStart() {
        super.onStart ();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener ( mAuthStateListener );
        //updateUI ( mAuth.getCurrentUser () );
    }

    @Override
    protected void onStop() {
        super.onStop ();
        if (mAuthStateListener != null) {
            mAuth.removeAuthStateListener ( mAuthStateListener );
        }
    }

    private void setupViewPager() {
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter ( getSupportFragmentManager () );
        sectionsPagerAdapter.addFragment ( new CameraFragment () );
        sectionsPagerAdapter.addFragment ( new HomeFragment () );
        sectionsPagerAdapter.addFragment ( new MessageFragment () );
        ViewPager viewPager = ( ViewPager ) findViewById ( R.id.container );
        viewPager.setAdapter ( sectionsPagerAdapter );
        TabLayout tabLayout = ( TabLayout ) findViewById ( R.id.Tabs );
        tabLayout.setupWithViewPager ( viewPager );

        tabLayout.getTabAt ( 0 ).setIcon ( R.drawable.ic_camera );
        tabLayout.getTabAt ( 1 ).setIcon ( R.drawable.ic_insta );
        tabLayout.getTabAt ( 2 ).setIcon ( R.drawable.ic_arrow );

    }

    private void setUpBottomNavigationView() {

        BottomNavigationViewEx bottomNavigationViewEx;
        bottomNavigationViewEx = ( BottomNavigationViewEx ) findViewById ( R.id.bottomNavViewBar );
        BottomNavigationViewHelper.setUpBottomNavigationView ( bottomNavigationViewEx );
        BottomNavigationViewHelper.enableNavigation ( mContex, bottomNavigationViewEx );
        Menu menu = bottomNavigationViewEx.getMenu ();
        MenuItem menuItem = menu.getItem ( ACTIVITY_NUMBER );
        menuItem.setChecked ( true );


    }

    private void initImageLoader() {
        UnivarsalImageLoader univarsalImageLoader = new UnivarsalImageLoader ( mContex );
        ImageLoader.getInstance ().init ( univarsalImageLoader.getConfig () );
    }
}
