package c.instagram.insta.Home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import c.instagram.insta.Profile.AccountSettingActivity;
import c.instagram.insta.Profile.EditProfileFragment;
import c.instagram.insta.Profile.SignOutFragment;
import c.instagram.insta.R;
import c.instagram.insta.Utils.BottomNavigationViewHelper;
import c.instagram.insta.Utils.SectionStatePagerAdapter;

/**
 * Created by h.khurshid on 1/18/2018.
 */

public class HomeTabFragment extends Fragment {
    private static final int ACTIVITY_NUMBER = 0;
    private Context mContex = getContext ();
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private static final String TAG = "HomeTabFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView ( inflater, container, savedInstanceState );

    }
}

