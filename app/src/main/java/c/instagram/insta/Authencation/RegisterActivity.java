package c.instagram.insta.Authencation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import c.instagram.insta.Home.HomeActivity;
import c.instagram.insta.R;
import c.instagram.insta.Utils.FirebasedMethods;
import c.instagram.insta.Utils.StringManipulation;

/**
 * Created by h.khurshid on 12/21/2017.
 */

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "Register Activity";
    private Context mContex = RegisterActivity.this;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private ProgressBar progressBar;
    private EditText EtEmail, Etpass,Etfullnaem ;
    private TextView aWaiting;
    private AppCompatButton btnRegister;
    private FirebasedMethods fBM= new FirebasedMethods ( mContex );
    private FirebaseDatabase mDb;
    private DatabaseReference mRef;
    private String append="";
    private String fullname,password,email;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_register );
        initWidgets();
        setUpFireBase ();

       btnSignup_Click();
    }

    private void initWidgets(){
        progressBar = ( ProgressBar ) findViewById ( R.id.progressBar );
        aWaiting = ( TextView ) findViewById ( R.id.tvWait );
        aWaiting.setVisibility ( View.GONE );
        progressBar.setVisibility ( View.GONE );
        btnRegister = ( AppCompatButton ) findViewById ( R.id.btnRegister );
        EtEmail = ( EditText ) findViewById ( R.id.inputEmailReg );
        Etpass = ( EditText ) findViewById ( R.id.inputPasswordReg );
        Etfullnaem = ( EditText ) findViewById ( R.id.inputFullName );


    }
    public void btnSignup_Click() {

        btnRegister.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(final View view) {
                aWaiting.setVisibility ( View.VISIBLE );
                progressBar.setVisibility ( View.VISIBLE );
                fullname = Etfullnaem.getText ().toString ().trim ();
                password = Etpass.getText ().toString ().trim ();
                email = EtEmail.getText ().toString ().trim ();

                if (!email.equals ( "" ) && !fullname.equals ( "" ) && !fullname.equals ( "" )) {

                        fBM.registerEmail (email, password);
                   }

                 else {
                    aWaiting.setVisibility ( View.GONE );
                    progressBar.setVisibility ( View.GONE );

                    Snackbar.make ( view, "Input paramenters are not correct", Snackbar.LENGTH_LONG )
                            .show ();
                    //  Toast.makeText ( mContex,"Input paramenters are not correct",Toast.LENGTH_LONG ).show ();


                }
            }
        } );




    }


    private void setUpFireBase() {
        mAuth = FirebaseAuth.getInstance ();
        mDb=FirebaseDatabase.getInstance ();
        mRef=mDb.getReference ();
        mAuthStateListener = new FirebaseAuth.AuthStateListener () {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser ();
                //  updateUI ( user );
                if (user != null) {

                    Log.d ( TAG, "onAuthStateChanged: Sign in :  " + user.getUid () );
                    mRef.addListenerForSingleValueEvent ( new ValueEventListener () {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            checkIfUsernameExist(Etfullnaem.getText ().toString ());
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    } );
                    finish ();
                } else {
                    Log.d ( TAG, "onAuthStateChanged: sign out" );
                }
            }
        };
    }

    private void checkIfUsernameExist(final String userUsername) {
        DatabaseReference reference=FirebaseDatabase.getInstance ().getReference ();
        Query query=reference
                .child ( getString ( R.string.dbNames_user ) )
                .orderByChild (getString (  R.string.field_username  ))
                .equalTo(userUsername);
        query.addListenerForSingleValueEvent ( new ValueEventListener () {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists ()){
                    for (DataSnapshot ds : dataSnapshot.getChildren ())
                    {
                        if(ds.exists ()){
                            append=mRef.push ().getKey ().substring ( 3,10 );
                        }
                    }

                }
                String mUsernaem="";
                mUsernaem=userUsername+append;
                Log.d ( TAG," Fullname ------------------   "+fullname );
                fBM.addNewUser ( email,mUsernaem,mUsernaem,0,"","",0,0,0,"");
                Toast.makeText ( mContex,"Signup Sucessfull. Sending verification email.",Toast.LENGTH_LONG ).show ();
                mAuth.signOut ();



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        } );
    }


    @Override
    public void onStart() {
        super.onStart ();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener ( mAuthStateListener );

    }

    @Override
    protected void onStop() {
        super.onStop ();
        if (mAuthStateListener != null) {
            mAuth.removeAuthStateListener ( mAuthStateListener );
        }
    }
}
