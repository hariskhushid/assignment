package c.instagram.insta.Authencation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import c.instagram.insta.Apis.Connection;
import c.instagram.insta.Apis.ServiceUrls;
import c.instagram.insta.Home.HomeActivity;
import c.instagram.insta.R;

/**
 * Created by h.khurshid on 12/21/2017.
 */

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "Login Activity";
    private Context mContex = LoginActivity.this;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private ProgressBar progressBar;
    private EditText email, password;
    private TextView aWaiting;
    private AppCompatButton btnLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_login );
        progressBar = ( ProgressBar ) findViewById ( R.id.progressBar );
        aWaiting = ( TextView ) findViewById ( R.id.tvWait );
        aWaiting.setVisibility ( View.GONE );
        progressBar.setVisibility ( View.GONE );
       setUpFireBase ();
        btnLogin_Click();
    }

    public void btnLogin_Click() {
        btnLogin = ( AppCompatButton ) findViewById ( R.id.btnLogin );
        email = ( EditText ) findViewById ( R.id.inputEmail );
        password = ( EditText ) findViewById ( R.id.inputPassword );
        btnLogin.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(final View view) {
             //   aWaiting.setVisibility ( View.VISIBLE );
             //   progressBar.setVisibility ( View.VISIBLE );
                String Usremail = email.getText ().toString ().trim ();
                String UsrPswd = password.getText ().toString ().trim ();
                if (!Usremail.equals ( "" ) && !UsrPswd.equals ( "" )) {

                 mAuth.signInWithEmailAndPassword ( Usremail,UsrPswd ).addOnCompleteListener ( new OnCompleteListener<AuthResult> () {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                          FirebaseUser user =mAuth.getInstance ().getCurrentUser ();



                            if(!task.isSuccessful ()){
                                //Toast.makeText ( mContex,"Not Sucessfull login",Toast.LENGTH_LONG ).show ();
                                Snackbar.make ( view, "Invalid username or password", Snackbar.LENGTH_LONG )
                                        .show ();
                                aWaiting.setVisibility ( View.GONE );
                                progressBar.setVisibility ( View.GONE );

                            }else {
                                try {
                                    if(user.isEmailVerified ()){
                                        Snackbar.make ( view, "WEllcome ", Snackbar.LENGTH_LONG )
                                                .show ();
                                        Log.d ( TAG, "onComplete: email is verified" );
                                        mContex.startActivity ( new Intent (mContex, HomeActivity.class ) );
                                    }
                                    else{
                                        Toast.makeText ( mContex,"Please verify your email",Toast.LENGTH_LONG ).show ();
                                        aWaiting.setVisibility ( View.GONE );
                                        progressBar.setVisibility ( View.GONE );
                                        mAuth.signOut ();
                                    }
                                }catch (NullPointerException e){
                                    Log.e ( TAG, "onComplete: NullPointerException" + e.getMessage ()  );
                                }

                            }
                        }
                    } );
                } else {
                    aWaiting.setVisibility ( View.GONE );
                    progressBar.setVisibility ( View.GONE );

                    Snackbar.make ( view, "Input paramenters are not correct", Snackbar.LENGTH_LONG )
                           .show ();
                  //  Toast.makeText ( mContex,"Input paramenters are not correct",Toast.LENGTH_LONG ).show ();


                }
            }
        } );

        TextView linkRegister=(TextView) findViewById ( R.id.link_signup );
        linkRegister.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                Intent intent
                        =new Intent ( mContex, RegisterActivity.class );
                startActivity ( intent );

            }
        } );

        if(mAuth.getCurrentUser ()!=null){
            Intent intent
                    =new Intent ( mContex, HomeActivity.class );
            startActivity ( intent );
            finish ();
        }
    }

    private void getLoginDb(final String email, final String password){
        String tag_json_obj = "getLoginDb";


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest( Request.Method.POST,
                ServiceUrls.LoginUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        Connection.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void setUpFireBase() {
        mAuth = FirebaseAuth.getInstance ();
        mAuthStateListener = new FirebaseAuth.AuthStateListener () {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser ();
              //  updateUI ( user );
                if (user != null) {
                    Log.d ( TAG, "onAuthStateChanged: Sign in :  " + user.getUid () );
                } else {
                    Log.d ( TAG, "onAuthStateChanged: sign out" );
                }
            }
        };
    }




    @Override
    public void onStart() {
        super.onStart ();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener ( mAuthStateListener );

    }

    @Override
    protected void onStop() {
        super.onStop ();
        if (mAuthStateListener != null) {
            mAuth.removeAuthStateListener ( mAuthStateListener );
        }
    }

}
