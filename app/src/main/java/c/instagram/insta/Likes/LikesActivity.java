package c.instagram.insta.Likes;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import c.instagram.insta.R;
import c.instagram.insta.Utils.BottomNavigationViewHelper;

/**
 * Created by h.khurshid on 12/19/2017.
 */

public class LikesActivity extends AppCompatActivity {


    private Context mContex=LikesActivity.this;
    private static final int ACTIVITY_NUMBER=3;
    @Override
    protected void onCreate(@Nullable Bundle saveInstanceState){
        super.onCreate ( saveInstanceState );
        setContentView ( R.layout.activity_home );
        //setUpBottomNavigationView ();
    }
    private void setUpBottomNavigationView(){

        BottomNavigationViewEx bottomNavigationViewEx=(BottomNavigationViewEx) findViewById ( R.id.bottomNavViewBar );
        BottomNavigationViewHelper.setUpBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation ( mContex,bottomNavigationViewEx );
        Menu menu= bottomNavigationViewEx.getMenu();
        MenuItem menuItem=menu.getItem(ACTIVITY_NUMBER);
        menuItem.setChecked ( true );

    }
}
