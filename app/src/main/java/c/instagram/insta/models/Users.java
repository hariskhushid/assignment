package c.instagram.insta.models;

/**
 * Created by h.khurshid on 12/25/2017.
 */

public class Users {
    private String userId;
    private String email;
    private long phone_number;
    private String username;



    private String full_name;

    public Users(String userId, String email,String username,String full_name, long phone_number) {
        this.userId = userId;
        this.email = email;
        this.phone_number = phone_number;
        this.username = username;
        this.full_name=full_name;
    }
public Users(){

}
    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone_number(long phone_number) {
        this.phone_number = phone_number;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {

        return userId;
    }

    public String getEmail() {
        return email;
    }

    public long getPhone_number() {
        return phone_number;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId='" + userId + '\'' +
                ", email='" + email + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", username='" + username + '\'' +
                '}';
    }


}
