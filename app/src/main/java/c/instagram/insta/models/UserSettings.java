package c.instagram.insta.models;

/**
 * Created by h.khurshid on 12/27/2017.
 */

public class UserSettings {
    private Users user;
    private user_account_settings settings;

    public UserSettings(Users user, user_account_settings settings) {
        this.user = user;
        this.settings = settings;
    }
    public UserSettings(){}

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public user_account_settings getSettings() {
        return settings;
    }

    public void setSettings(user_account_settings settings) {
        this.settings = settings;
    }
}
