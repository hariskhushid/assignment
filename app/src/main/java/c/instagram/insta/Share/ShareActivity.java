package c.instagram.insta.Share;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import c.instagram.insta.R;
import c.instagram.insta.Utils.BottomNavigationViewHelper;
import c.instagram.insta.Utils.SectionsPagerAdapter;
import c.instagram.insta.Utils.UserPermissions;

/**
 * Created by h.khurshid on 12/19/2017.
 */

public class ShareActivity extends AppCompatActivity {


    private static final int ACTIVITY_NUMBER = 2;
    public static final int VERIFY_PERMISISON_REQUEST=1;
    private Context mContex = ShareActivity.this;
    private ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle saveInstanceState) {
        super.onCreate ( saveInstanceState );
        setContentView ( R.layout.activity_share );

        if(checkPermissionArray ( UserPermissions.PERMISSIONS )){
            setupViewPager();

        }else{
            verifyPermissions(UserPermissions.PERMISSIONS);
        }

//       / setUpBottomNavigationView ();
    }
    private void setupViewPager() {
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter ( getSupportFragmentManager () );
        sectionsPagerAdapter.addFragment ( new GalleryFragment () );
        sectionsPagerAdapter.addFragment ( new PhotoFragment () );

         viewPager = ( ViewPager ) findViewById ( R.id.container );
        viewPager.setAdapter ( sectionsPagerAdapter );
        TabLayout tabLayout = ( TabLayout ) findViewById ( R.id.tabsBottom );
        tabLayout.setupWithViewPager ( viewPager );

        tabLayout.getTabAt ( 0 ).setText ( R.string.gallery );
        tabLayout.getTabAt ( 1 ).setText ( R.string.photo );



    }

    public int getTabNumber(){
        return viewPager.getCurrentItem ();
    }
    private void verifyPermissions(String[] permissions) {
         ActivityCompat.requestPermissions ( ShareActivity.this,permissions,VERIFY_PERMISISON_REQUEST );

    }

    private boolean checkPermissionArray(String[] permissions) {
        for (int i=0;i<UserPermissions.PERMISSIONS.length;i++){
            String check=UserPermissions.PERMISSIONS[i];
            if(!checkPermission ( check )){
                return false;
            }

        }
        return true;
    }

    public boolean checkPermission(String permission) {
        int permisionRequest= ActivityCompat.checkSelfPermission ( ShareActivity.this,permission );
        if(permisionRequest!= PackageManager.PERMISSION_GRANTED){
            return false;
        }else{
            return true;
        }

    }

    private void setUpBottomNavigationView() {

        BottomNavigationViewEx bottomNavigationViewEx = ( BottomNavigationViewEx ) findViewById ( R.id.bottomNavViewBar );
        BottomNavigationViewHelper.setUpBottomNavigationView ( bottomNavigationViewEx );
        BottomNavigationViewHelper.enableNavigation ( mContex, bottomNavigationViewEx );
        Menu menu = bottomNavigationViewEx.getMenu ();
        MenuItem menuItem = menu.getItem ( ACTIVITY_NUMBER );
        menuItem.setChecked ( true );
    }
}
