package c.instagram.insta.Share;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import c.instagram.insta.R;
import c.instagram.insta.Utils.UserPermissions;

/**
 * Created by h.khurshid on 1/18/2018.
 */

public class PhotoFragment extends Fragment {

    private Button btnCam;
    private static final int PHOTO_FRAGMENT_NUM=1;
    private static final int CAMERA_REQUEST_CODE=5;
    private static final int GALLERY_FRAGMENT_NUM=2;
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view=inflater.inflate ( R.layout.fragment_photo,container,false );
            btnCam=(Button) view.findViewById ( R.id.btnLaunchCamera );
            btnCam.setOnClickListener ( new View.OnClickListener () {
                @Override
                public void onClick(View view) {
                    if(((ShareActivity) getActivity ()).getTabNumber ()==PHOTO_FRAGMENT_NUM){

                        if(((ShareActivity)getActivity ()).checkPermission ( UserPermissions.CAM_PERMISSIONS[0] )){
                            Intent camIntent= new Intent ( MediaStore.ACTION_IMAGE_CAPTURE );
                            startActivityForResult ( camIntent,CAMERA_REQUEST_CODE );
                        }
                        else{
                            Intent intent= new Intent (getActivity (),ShareActivity.class);
                            intent.setFlags ( Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK );
                            startActivity ( intent );
                        }
                    }
                }
            } );
            return view;
        }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult ( requestCode, resultCode, data );
        if (requestCode==CAMERA_REQUEST_CODE){

        }
    }
}
