package c.instagram.insta.Share;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import c.instagram.insta.R;
import c.instagram.insta.Utils.FilePaths;
import c.instagram.insta.Utils.FileSearch;
import c.instagram.insta.Utils.GridImageAdapter;

/**
 * Created by h.khurshid on 1/18/2018.
 */

public class GalleryFragment extends Fragment {
    private static final int NUM_GRID_COL = 3;
    private GridView gridView;
    private ImageView galleryImage;
    private ProgressBar progressBar;
    private Spinner directorySpinner;
    private ArrayList<String> directories;
    private String mAppend="file:/";
    private String mSelectedImage="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate ( R.layout.fragment_gallery, container, false );
        galleryImage = ( ImageView ) view.findViewById ( R.id.galleryImageView );
        gridView = ( GridView ) view.findViewById ( R.id.gridView );
        progressBar = ( ProgressBar ) view.findViewById ( R.id.progressBar );
        directorySpinner = ( Spinner ) view.findViewById ( R.id.spinnerDirectory );
        progressBar.setVisibility ( View.GONE );
        directories = new ArrayList<> ();
        ImageView shareClose = ( ImageView ) view.findViewById ( R.id.ivCloseShare );
        shareClose.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                getActivity ().finish ();
            }
        } );
        TextView nxtScreen = ( TextView ) view.findViewById ( R.id.tvNext );
        nxtScreen.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent ( getActivity (),NextActivity.class );
                intent.putExtra ( getString ( R.string.selected_image ),mSelectedImage );
                startActivity ( intent );
            }
        } );
        init ();
        return view;
    }

    public void init() {
        FilePaths filePaths = new FilePaths ();
        if (FileSearch.getDirectoryPath ( filePaths.ROOT_DIR ) != null) {
            directories = (FileSearch.getDirectoryPath ( filePaths.ROOT_DIR ));
        }
        ArrayList<String> directoryNames= new ArrayList<> (  );
        for (int i=0;i<directories.size ();i++){
            int index=directories.get ( i ).lastIndexOf ( "/" );
            String string=directories.get ( i ).substring ( index );
            directoryNames.add ( string );
        }
        directories.add ( filePaths.ROOT_DIR );
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String> ( getActivity ()
                , android.R.layout.simple_spinner_item, directoryNames
        );
        arrayAdapter.setDropDownViewResource ( android.R.layout.simple_spinner_dropdown_item );
        directorySpinner.setAdapter ( arrayAdapter );
        directorySpinner.setOnItemSelectedListener ( new AdapterView.OnItemSelectedListener () {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setUpGridView ( directories.get ( i ) );

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        } );
    }

    private void setUpGridView(String selectedDirectory) {
        final ArrayList<String> imgURls = FileSearch.getFilesPath ( selectedDirectory );
        int gridWidth = getResources ().getDisplayMetrics ().widthPixels;
        int imgWidth = gridWidth / NUM_GRID_COL;
        gridView.setColumnWidth ( imgWidth );
        GridImageAdapter imageAdapter= new GridImageAdapter (
                getActivity (),R.layout.layout_grid_image_view,mAppend,imgURls
        );
        gridView.setAdapter ( imageAdapter );
        setImage(imgURls.get ( 0 ),galleryImage,mAppend);
        mSelectedImage=imgURls.get (  0);
        gridView.setOnItemClickListener ( new AdapterView.OnItemClickListener () {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                setImage ( imgURls.get ( i ),galleryImage,mAppend );
                mSelectedImage=imgURls.get (  i);
            }
        } );


    }

    private void setImage(String imgUrls, ImageView image, String append) {
        ImageLoader loader= ImageLoader.getInstance ();
        loader.displayImage ( append + imgUrls, image, new ImageLoadingListener () {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressBar.setVisibility ( View.VISIBLE );
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.INVISIBLE);
        }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        } );
    }
}
