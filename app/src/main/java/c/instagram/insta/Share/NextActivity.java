package c.instagram.insta.Share;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import c.instagram.insta.Authencation.LoginActivity;
import c.instagram.insta.R;
import c.instagram.insta.Utils.FirebasedMethods;
import c.instagram.insta.Utils.UnivarsalImageLoader;

/**
 * Created by h.khurshid on 1/19/2018.
 */

public class NextActivity extends AppCompatActivity {
    private Context mContext;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseDatabase mDb;
    private DatabaseReference mRef;
    // private Context mContex = getActivity ();
    private FirebasedMethods fBm;
    private final static String TAG="NextActivity";
    private String mAppend="file:/";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_next );
        setUpFireBase ();
        ImageView backArrow = ( ImageView ) findViewById ( R.id.ivCloseNext );
        backArrow.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
               finish ();
            }
        } );
        TextView tvShare=(TextView) findViewById ( R.id.tvShare );
        tvShare.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
            }
        } );
        setImage ();
    }
    private void setImage(){
        ImageView imageView = (ImageView) findViewById ( R.id.imageShare );
        UnivarsalImageLoader.setImage ( getIntent ().getStringExtra ( getString (R.string.selected_image )),imageView,null,mAppend );

    }
    private void setUpFireBase() {
        mAuth = FirebaseAuth.getInstance ();
        mDb = FirebaseDatabase.getInstance ();
        mRef = mDb.getReference ();
        mAuthStateListener = new FirebaseAuth.AuthStateListener () {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser ();
                updateUI ( user );

                if (user != null) {
                    Log.d ( TAG, "onAuthStateChanged: Sign in :  " + user.getUid () );
                } else {
                    Log.d ( TAG, "onAuthStateChanged: sign out" );
                }
            }
        };
        mRef.addValueEventListener ( new ValueEventListener () {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               // setProfileWidgets ( fBm.getUserSetting ( dataSnapshot ) );
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        } );
    }


    private void updateUI(FirebaseUser user) {
        if (user == null) {

            Toast.makeText ( mContext, " no user found", Toast.LENGTH_LONG );
            Intent intent = new Intent ( mContext, LoginActivity.class );
            startActivity ( intent );

        }

    }

    @Override
    public void onStart() {
        super.onStart ();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener ( mAuthStateListener );
        //updateUI ( mAuth.getCurrentUser () );
    }

    @Override
    public void onStop() {
        super.onStop ();
        if (mAuthStateListener != null) {
            mAuth.removeAuthStateListener ( mAuthStateListener );
        }
    }
}
